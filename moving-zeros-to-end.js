/*
Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.

moveZeros([false,1,0,1,2,0,1,3,"a"]) // returns[false,1,1,2,1,3,"a",0,0]
*/

function moveZeros(arr) {
    const EL_TO_COMPARE = 0;
    const tempZeroHolder = []

    for(let i = 0; i < arr.length; i++){
        if(arr[i] === EL_TO_COMPARE){
            tempZeroHolder.push(EL_TO_COMPARE)           
            arr.splice(i,1)
            i--
        }
    }
    return arr.concat(tempZeroHolder)
}

// const array = [false,1,0,1,2,0,1,3,"a"];
// const array = [ 9, +0, 9, 1, 2, 1, 1, 3, 1, 9, +0, +0, 9, +0, +0, +0, +0, +0, +0, +0 ];
// const array = [ {}, null, '8', false, '3', false, [], {}, null, {}, +0, null, {}, [], '7', '5', null, 3, 4, {}, 3, 6, false, +0, +0, +0 ];
const array = [ '5', 5, [], null, 6, '3', '3', 4, '0', 4, '9', 1, null, +0, 9, '0', 5, true, null, 9, 4, '9', '0', +0, +0, +0, +0 ];

// console.log(moveZeros(array))

//second solution
function moveZeros(arr){
    return arr.filter(notZeroes => notZeroes !== 0).concat(arr.filter(zeros => zeros === 0))
}

console.log(moveZeros(array))