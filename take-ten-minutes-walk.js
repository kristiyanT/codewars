/*
You live in the city of Cartesia where all roads are laid out in a perfect grid.
You arrived ten minutes too early to an appointment, so you decided to take the
opportunity to go for a short walk. The city provides its citizens with a Walk
Generating App on their phones -- everytime you press the button it sends you an
array of one-letter strings representing directions to walk (eg. ['n', 's', 'w', 'e']).
You always walk only a single block for each letter (direction) and you know it takes
you one minute to traverse one city block, so create a function that will return true
if the walk the app gives you will take you exactly ten minutes (you don't want to be
early or late!) and will, of course, return you to your starting point. Return false otherwise.

Note: you will always receive a valid array containing a random assortment of direction
letters ('n', 's', 'e', or 'w' only). It will never give you an empty array (that's not a walk, that's standing still!).
*/

/*
one letter takes 1 minute
if arr length is less than 10 return false

['n','s','n','s','n','s','n','s','n','s'] - true
['n','s','e','w','n','s','e','w','n','s'] - true
['n','n','n','s','n','s','n','s','n','s'] - false
['w','e','w','e','w','e','w','e','w','e','w','e'] - false
*/

// function isValidWalk(walk) {
//     const STATIC_ARR_LENGTH = 10;

//     let countN = 0;
//     let countS = 0;
//     let countE = 0;
//     let countW = 0;

//     if(walk.length !== STATIC_ARR_LENGTH){
//         return false
//     }

//     walk.forEach(direction => {
//         if(direction === 'n'){
//             countN++
//         }else if(direction === 's'){
//             countS++
//         }else if(direction === 'e'){
//             countE++
//         }else{
//             countW++
//         }
//     })

//     return countN === countS && countE === countW    
// }

// optimized

function isValidWalk(walk) {
    const STATIC_ARR_LENGTH = 10;

    const countDirection = (str) => {
        return walk.filter((direction) => direction === str).length
    }

    return walk.length === STATIC_ARR_LENGTH && countDirection('n') === countDirection('s') && countDirection('e') === countDirection('w')
}

console.log(isValidWalk(['w','e','w','e','w','e','w','e','w','e']))